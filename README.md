# morse

Encode and decode english morse.

### Dependencies

- purescript-v0.13.8
- specular-0.8.1
- spago
- parcel

### How to Build

```sh
spago build
parcel build --public-url ./ --out-dir dist dist/index.html
```

And an http server on the `dist/` directory.
