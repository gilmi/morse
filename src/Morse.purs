module Morse where

import Prelude

import Data.Array (drop, take)
import Data.Array as A
import Data.Either (Either(..))
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Eq (genericEq)
import Data.Generic.Rep.Show (genericShow)
import Data.List (List(..), catMaybes, concat, dropWhile, fromFoldable, head, null, reverse, singleton, toUnfoldable, (:))
import Data.Map (Map)
import Data.Map as M
import Data.Maybe (Maybe(Nothing), maybe)
import Data.String.CodeUnits (fromCharArray, toCharArray)
import Data.String (toLower)
import Data.Traversable (traverse)
import Data.Tuple (Tuple(..))


data Bit
  = Dit
  | Dah
  | Pause
  | Newline

derive instance genericBit :: Generic Bit _

instance showBit :: Show Bit where
  show = genericShow

instance eqBit :: Eq Bit where
  eq = genericEq
  

data MorseTree
  = Node Char MorseTree MorseTree
  | Leaf


type Morse
  = List Bit


morseMap :: MorseTree
morseMap =
  Node ' '
    ( Node 'e'
      ( Node 'i'
        ( Node 's'
          ( Node 'h'
            ( Node '5' Leaf Leaf )
            ( Node '4' Leaf Leaf )
          )
          ( Node 'v'
            Leaf
            ( Node '3' Leaf Leaf )
          )
        )
        ( Node 'u'
          ( Node 'f' Leaf Leaf
          )
          ( Node '-'
            Leaf
            ( Node '2' Leaf Leaf )
          )
        )
      )
      ( Node 'a'
        ( Node 'r'
          ( Node 'l' Leaf Leaf )
          Leaf
        )
        ( Node 'w'
          ( Node 'p' Leaf Leaf )
          ( Node 'j'
            Leaf
            ( Node '1' Leaf Leaf )
          )
        )
      )
    )
    ( Node 't'
      ( Node 'n'
        ( Node 'd'
          ( Node 'b'
            ( Node '6' Leaf Leaf )
            Leaf
          )
          ( Node 'x' Leaf Leaf )
        )
        ( Node 'k'
          ( Node 'c' Leaf Leaf )
          ( Node 'y' Leaf Leaf )
        )
      )
      ( Node 'm'
        ( Node 'g'
          ( Node 'z'
            ( Node '7' Leaf Leaf )
            Leaf
          )
          ( Node 'q' Leaf Leaf )
        )
        ( Node 'o'
          ( Node '.'
            ( Node '8' Leaf Leaf )
            Leaf
          )
          ( Node '-'
            ( Node '9' Leaf Leaf )
            ( Node '0' Leaf Leaf )
          )
        )
      )
    )


decode' :: MorseTree -> Morse -> Maybe (List Char)
decode' tree code = case Tuple code tree of
  Tuple _ Leaf ->
    Nothing
  Tuple Nil (Node l _ _) ->
    pure (singleton l)
  Tuple (Cons Pause rest) (Node l _ _) ->
    (\result -> l : result) <$> decode' morseMap rest
  Tuple (Cons Newline Nil) (Node ' ' _ _) ->
    pure $ singleton '\n'
  Tuple (Cons Newline Nil) (Node l _ _) ->
    pure $ l : singleton '\n'
  Tuple (Cons Newline rest) (Node ' ' _ _) ->
    (\result -> '\n' : result) <$> decode' morseMap rest
  Tuple (Cons Newline rest) (Node l _ _) ->
    (\result -> l : '\n' : result) <$> decode' morseMap rest
  Tuple (Cons Dit rest) (Node _ subtree _) ->
    decode' subtree rest
  Tuple (Cons Dah rest) (Node _ _ subtree) ->
    decode' subtree rest

parse :: String -> Either String Morse
parse = toCharArray >>> fromFoldable >>> \str -> flip traverse str \c -> case c of
  '.' -> pure Dit
  '-' -> pure Dah
  ' ' -> pure Pause
  '\n' -> pure Newline
  '\t' -> pure Pause
  '\r' -> pure Pause
  _   -> Left $ "Unexpected character " <> show c

letterMap :: Map Char Morse
letterMap =
  (<>) ('\n' `M.singleton` singleton Newline)
    $ M.fromFoldable $ catMaybes $ fromFoldable $ do
      d1 <- [Dit, Dah, Pause]
      d2 <- [Dit, Dah, Pause]
      d3 <- [Dit, Dah, Pause]
      d4 <- [Dit, Dah, Pause]
      d5 <- [Dit, Dah, Pause]
      d6 <- [Dit, Dah, Pause]
      let
        morse = trim $ fromFoldable $ [d1, d2, d3, d4, d5, d6]
      pure $ flip Tuple morse <$> (head =<< decode' morseMap morse)

trim :: Morse -> Morse
trim =
  reverse
    <<< Cons Pause
    <<< dropWhile ((==) Pause)
    <<< reverse
    <<< dropWhile ((==) Pause)

encode :: String -> Either String Morse
encode str = map concat $ toLower str # toCharArray >>> fromFoldable >>> traverse \l ->
  maybe
    (Left $ "Unexpected letter " <> show l)
    pure
    $ M.lookup l
    $ letterMap

decode :: String -> Either String String
decode morse =
  pure
    <<< fromCharArray
    <<< toUnfoldable
    =<< (\m -> if null m then pure Nil else maybe (Left "Invalid morse code.") pure <<< decode' morseMap $ m)
    =<< parse morse

ppBit :: Bit -> Char
ppBit = case _ of
  Dit -> '.'
  Dah -> '-'
  Pause -> ' '
  Newline -> '\n'

ppMorse :: Morse -> String
ppMorse = fromCharArray <<< A.reverse <<< trimEnd <<< A.reverse <<< toUnfoldable <<< map ppBit
  where
    trimEnd str = case take 1 str of
      [' '] -> drop 1 str
      _ -> str

