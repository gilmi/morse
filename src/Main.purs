module Main where

import Prelude

import Effect (Effect)
import Data.Either (Either, either)
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Morse (decode, encode, ppMorse)
import Specular.Dom.Builder.Class (domEventWithSample, dynText, el, elAttr, elDynAttr', text)
import Specular.Dom.Node.Class (Attrs, (:=))
import Specular.Dom.Widget (class MonadWidget, runMainWidgetInBody)
import Specular.Dom.Widgets.Input (getTextInputValue, setTextInputValue)
import Specular.FRP (Dynamic, Event, WeakDynamic, changedW, filterMapEvent, fixFRP, fixFRP_, holdUniqDynBy, leftmost, subscribeEvent_)

main :: Effect Unit
main = runMainWidgetInBody mainWidget

mainWidget :: forall m. MonadWidget m => m Unit
mainWidget = widget


widget :: forall m. MonadWidget m => m Unit
widget = do
  elAttr "div" ("style" := "width: 90%; margin:auto;") $ fixFRP_ \omega -> do
    el "h1" $ text "English -> Morse"
    e2m <- engToMorseWidget omega.m2e
    el "h1" $ text "Morse -> English"
    m2e <- morseToEngWidget omega.e2m
    pure {e2m, m2e}


morseToEngWidget :: forall m. MonadWidget m => WeakDynamic String -> m (Dynamic String)
morseToEngWidget e2mDyn =
  fixFRP $ convertWidget decode (pure <<< ppMorse <=< encode) e2mDyn

engToMorseWidget :: forall m. MonadWidget m => WeakDynamic String -> m (Dynamic String)
engToMorseWidget m2eDyn =
  fixFRP $ convertWidget encode decode m2eDyn


convertWidget
  :: forall m x. MonadWidget m
  => (String -> Either String x)
  -> (String -> Either String String)
  -> WeakDynamic String
  -> { myConvertDyn :: WeakDynamic String }
  -> m (Tuple { myConvertDyn :: Dynamic String } (Dynamic String))
convertWidget checker convert otherConvertDyn omega = do
  elAttr "pre" ("style" := "color: red; font-weight: bold; font-size: 1.5vw")
    $ dynText $ map (either identity (const "") <<< checker) omega.myConvertDyn
  myConvertDyn <-
    textbox
      $ filterMapEvent (either (const Nothing) pure <<< convert)
      $ changedW otherConvertDyn
  pure $ Tuple {myConvertDyn} myConvertDyn


textbox :: forall m. MonadWidget m => (Event String) -> m (Dynamic String)
textbox changer = el "div" $ do
  textarea "" ("style" := "width: 95%; height: 15%; font-size: 3vw;") changer


textarea :: forall m. MonadWidget m => String -> Attrs -> Event String -> m (Dynamic String)
textarea initial attrs strE = do
  Tuple node _ <- elDynAttr' "textarea" (pure attrs) (text initial)
  subscribeEvent_ (setTextInputValue node) strE
  changed1 <- domEventWithSample (\_ -> getTextInputValue node) "input" node
  let
    changed =
      leftmost
        [ changed1
        , strE
        ]
  holdUniqDynBy (==) initial changed
